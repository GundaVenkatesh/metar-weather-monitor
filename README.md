# Metar Weather Monitor Application 

## Requirements:
*	The app should be written 100% in Java (we appreciate Kotlin too, but are focussing on pure Java for this particular task).
*	Only the Google APIs and official libraries from Google are allowed (no 3rd party libraries, because we would like understand how you would solve a problem with a standard tool set).
*	METAR reports for particular airport(s) should be easily accesible through the UI.
*	The app should also be useable offline. Whenever it is online, the cache should be updated and kept in sync with the METAR database. Keep in mind that METARs are updated regularly during the day.
*	Your code should be structured and packaged with highest maintainability in mind.

## Requirements Breakdown:
* Kotlin should not be used.
* Retrofit should not be used. 
* To use in offline, the last data should be stored in the database(use ROOM for database).
* Use ViewModel to update UI if some thing is changed in the database.
* To Get data from the Metar Database use legacy https.
* Only get German aiports(Station starting with ED.
* Create list with German airport in the application to easily access the information.
* Model(Room)-ViewModel(LiveData)-UI(Actvity/Fragment) structure.

## Prototype: 
The Prototypes are create in Adobe XD.

If images are not shown, Press check prototype folder for images. 

![Main](prototype/main.PNG)
![DetailScreen](prototype/detailScreen.PNG)

## Release
Please find the release at app/release/app-release.apk

