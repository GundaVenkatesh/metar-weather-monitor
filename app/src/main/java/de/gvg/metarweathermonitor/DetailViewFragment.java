package de.gvg.metarweathermonitor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import de.gvg.metarweathermonitor.controller.WeatherMonitorController;
import de.gvg.metarweathermonitor.model.AirportWeatherData;
import de.gvg.metarweathermonitor.viewmodel.AirportWeatherDataViewModel;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class DetailViewFragment extends Fragment {
    private TextView dateAndTimeView;
    private TextView locationTextView;
    private TextView temperatureTextView;
    private ImageView weatherImageView;
    private TextView windTextView;
    private TextView visibilityTextView;
    private TextView dewPointTextView;
    private TextView relativeHumidityTextView;
    private TextView pressureTextView;
    private TextView cycleTextView;
    private TextView obTextView;
    private ProgressBar loadingProgressBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_view, container, false);
        dateAndTimeView = view.findViewById(R.id.time_date_decoded_textView);
        locationTextView = view.findViewById(R.id.location_decoded_textView);
        temperatureTextView = view.findViewById(R.id.temp_decode_textView);

        weatherImageView = view.findViewById(R.id.weather_decoded_imageView);
        windTextView = view.findViewById(R.id.wind_decode_textView);
        visibilityTextView = view.findViewById(R.id.visibility_decode_textView);
        dewPointTextView = view.findViewById(R.id.dew_point_decode_textView);
        relativeHumidityTextView = view.findViewById(R.id.relative_humidity_decode_textView);
        pressureTextView = view.findViewById(R.id.pressure_decode_textView);
        cycleTextView = view.findViewById(R.id.cycle_decode_textView);
        obTextView = view.findViewById(R.id.ob_raw_textView);
        loadingProgressBar = view.findViewById(R.id.progressBar_detail_view);
        loadingProgressBar.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            String airPortCode = activity.getCurrentSelectedAirport();
            activity.getSupportActionBar().setTitle(airPortCode);
        }

        AirportWeatherDataViewModel viewModelProvider = new ViewModelProvider(this).get(AirportWeatherDataViewModel.class);
        viewModelProvider.getWeatherDataListLiveData().observe(this, new Observer<List<AirportWeatherData>>() {
            @Override
            public void onChanged(List<AirportWeatherData> airportWeatherData) {
                for (AirportWeatherData data : airportWeatherData) {
                    if (data.getAirportCode().equals(activity.getCurrentSelectedAirport())) {
                        prepareDetailView(data);
                    }
                }
            }

        });
    }

    @SuppressLint("SetTextI18n")
    private void prepareDetailView(@NonNull AirportWeatherData airportWeatherData) {
        dateAndTimeView.setText(airportWeatherData.getTime());
        locationTextView.setText(airportWeatherData.getLocation());
        windTextView.setText(airportWeatherData.getWind());
        relativeHumidityTextView.setText(airportWeatherData.getRelativeHumidity());
        visibilityTextView.setText(airportWeatherData.getVisibility());
        dewPointTextView.setText(airportWeatherData.getDewPoint());
        pressureTextView.setText(airportWeatherData.getPressure());
        cycleTextView.setText(airportWeatherData.getCycle());
        obTextView.setText(airportWeatherData.getRawData());

        String tempTotalString = airportWeatherData.getTemperature();
        if (tempTotalString != null) {
            try {
                String temperature = tempTotalString.split("\\(")[1].split("\\)")[0];

                temperatureTextView.setText(temperature.substring(0, temperature.length() - 1) + "°C");
            } catch (Exception e) {
                temperatureTextView.setText(tempTotalString);
            }
        }

        String skyCondition = airportWeatherData.getSkyConditions();
        String weather = airportWeatherData.getWeather();
        if (skyCondition == null) {
            weatherImageView.setImageResource(R.drawable.sun_clouds);
            return;
        }

        switch (skyCondition) {
            case SkyConditionConstants.OVERCAST:
                weatherImageView.setImageResource(R.drawable.dark_cloud);
                break;
            case SkyConditionConstants.OBSCURED:
                weatherImageView.setImageResource(R.drawable.dark_rain_light);
                break;
            case SkyConditionConstants.MOSTLY_CLOUDY:
                weatherImageView.setImageResource(R.drawable.clouds);
                break;
            case SkyConditionConstants.CLEAR:
            case SkyConditionConstants.MOSTLY_CLEAR:
                if (airportWeatherData.getTime() != null && airportWeatherData.getTime().contains("PM")) {
                    weatherImageView.setImageResource(R.drawable.moon);
                } else {
                    weatherImageView.setImageResource(R.drawable.sun);
                }

                break;
            case SkyConditionConstants.PARTY_CLOUDY:
            default:
                if (airportWeatherData.getTime() != null && airportWeatherData.getTime().contains("PM")) {
                    weatherImageView.setImageResource(R.drawable.moon_clouds);
                } else {
                    weatherImageView.setImageResource(R.drawable.sun_clouds);
                }
                break;
        }

        if (weather == null) {
            return;
        }

        if (weather.contains(SkyConditionConstants.RAIN)) {
            weatherImageView.setImageResource(R.drawable.cloud_rain);
        } else if (weather.contains(SkyConditionConstants.SNOW)) {
            weatherImageView.setImageResource(R.drawable.snow);
        } else if (weather.contains(SkyConditionConstants.FOG)) {
            weatherImageView.setImageResource(R.drawable.dark_cloud);
        } else if (skyCondition.contains(SkyConditionConstants.CLOUDS)) {
            weatherImageView.setImageResource(R.drawable.clouds);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.sync) {
            final MainActivity activity = (MainActivity) getActivity();
            if (activity == null) {
                return super.onOptionsItemSelected(item);
            }
            item.setEnabled(false);
            loadingProgressBar.setVisibility(View.VISIBLE);
            Utils.getController(activity.getApplication()).updateSpecificAirportWeatherData(activity.getCurrentSelectedAirport(), new WeatherMonitorController.OnDataLoadedListener() {
                @Override
                public void onDataLoaded() {
                    loadingProgressBar.setVisibility(View.GONE);
                    item.setEnabled(true);
                }

                @Override
                public void onDataLoadingException(Exception exception) {
                    loadingProgressBar.setVisibility(View.GONE);
                    item.setEnabled(true);
                    Toast.makeText(getActivity(), R.string.problem, Toast.LENGTH_SHORT).show();
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }
}