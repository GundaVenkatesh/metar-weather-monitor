package de.gvg.metarweathermonitor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import de.gvg.metarweathermonitor.controller.WeatherMonitorController;

/**
 * The MainActivity.
 */
public class MainActivity extends AppCompatActivity {
    private static String MAIN_ACTIVITY = "MainActivity";
    private String mCurrentSelectedAirport;
    private WeatherMonitorController weatherMonitorController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
     //   Utils.getWeatherMonitorController(this.getApplication()).deleteAirportsData();
        Utils.getController(this.getApplication()).updateAirportWeatherData(new WeatherMonitorController.OnDataLoadedListener() {
            @Override
            public void onDataLoaded() {
            }

            @Override
            public void onDataLoadingException(Exception exception) {
                Toast.makeText(getApplicationContext(), R.string.problem, Toast.LENGTH_SHORT).show();
            }
        });
        showMainFragment();
    }

    /**
     * It shows the Main Fragment.
     */
    void showMainFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container,new MainFragment());
        fragmentTransaction.commit();
    }

    /**
     * It shows the Detail view Fragment.
     */
    void showDetailViewFragment(){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container,new DetailViewFragment());
        fragmentTransaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        startBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopBroadcastReceiver();
    }

    /**
     * The method is used to start the broadcast receiver.
     * The Time Interval is 30 min.
     */
    private void startBroadcastReceiver(){
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this,UpdateReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,1216,intent,0);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,0,1000*60*30,pendingIntent);
    }

    /**
     * The method is used to stop broadcastReceiver.
     */
    private void stopBroadcastReceiver(){
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this,UpdateReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,1216,intent,0);
        alarmManager.cancel(pendingIntent);
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            showMainFragment();
            Log.i(MAIN_ACTIVITY, "Back button is pressed");
        } else if (item.getItemId() == R.id.sync) {
            Log.i(MAIN_ACTIVITY, "Synchronize button is pressed");

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showMainFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tool_bar_item, menu);
        return true;
    }

    /**
     * The method is used to set the current selected airport code from the list.
     * @param currentSelectedAirport The Selected airport code.
     */
    public void setCurrentSelectedAirport(String currentSelectedAirport){
        mCurrentSelectedAirport = currentSelectedAirport;
    }

    /**
     * The method is used to get current selected airport code.
     * @return The selected airport code.
     */
    public String getCurrentSelectedAirport() {
        return mCurrentSelectedAirport;
    }
}