package de.gvg.metarweathermonitor;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.gvg.metarweathermonitor.controller.WeatherMonitorController;
import de.gvg.metarweathermonitor.model.AirportWeatherData;
import de.gvg.metarweathermonitor.viewmodel.AirportWeatherDataViewModel;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    private RecyclerView recyclerView;
    private WeatherListAdapter adapter;
    private EditText searchEditText;
    private ProgressBar loadingProgressBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        searchEditText = view.findViewById(R.id.airport_code_editText);
        loadingProgressBar = view.findViewById(R.id.data_loading_progressBar);
        loadingProgressBar.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            activity.getSupportActionBar().setTitle(getActivity().getTitle());
        }
        adapter = new WeatherListAdapter(this.getContext());
        adapter.setOnItemClickListener(airportCode -> {

            if (activity != null) {
                activity.setCurrentSelectedAirport(airportCode);
                activity.showDetailViewFragment();
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        AirportWeatherDataViewModel viewModelProvider = new ViewModelProvider(this).get(AirportWeatherDataViewModel.class);
        viewModelProvider.getWeatherDataListLiveData().observe(this, new Observer<List<AirportWeatherData>>() {
            @Override
            public void onChanged(List<AirportWeatherData> airportWeatherData) {
                adapter.setWeatherList(airportWeatherData);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.sync) {
            Activity activity = getActivity();
            if (activity == null) {
                return super.onOptionsItemSelected(item);
            }
            item.setEnabled(false);
            loadingProgressBar.setVisibility(View.VISIBLE);
            Utils.getController(activity.getApplication()).updateAirportWeatherData(new WeatherMonitorController.OnDataLoadedListener() {
                @Override
                public void onDataLoaded() {
                    loadingProgressBar.setVisibility(View.GONE);
                    item.setEnabled(true);
                    adapter.notifyDataSetChanged();
                 }

                @Override
                public void onDataLoadingException(Exception exception) {
                    loadingProgressBar.setVisibility(View.GONE);
                    item.setEnabled(true);
                    Toast.makeText(getActivity(), R.string.problem, Toast.LENGTH_SHORT).show();
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

}