package de.gvg.metarweathermonitor;

public final class SkyConditionConstants {

    /**
     * The private constructor.
     */
    private SkyConditionConstants(){

    }

    static final String SANDSTORM = "sandstorm";
    static final String MOSTLY_CLOUDY = "mostly cloudy";
    static final String OVERCAST = "overcast";
    static final String MOSTLY_CLEAR = "mostly clear";
    static final String PARTY_CLOUDY = "partly cloudy";
    static final String CLEAR = "clear";
    static final String OBSCURED = "obscured";
    static final String LIGHT_CLOUD = "light snow";
    static final String HAZE = "haze";
    static final String MIST = "mist";
    static final String CLOUDS="clouds";
    static final String SNOW = "snow";
    static final String FOG = "fog";
    static final String PARTIAL_FOG = "partial fog";
    static final String LIGHT_DRIZZLE ="light drizzle";
    static final String RAIN="rain";
    static final String HEAVY_SNOW = "heavy snow";
    static final String CUMULONIMBUS = "Cumulonimbus clouds observed";
}
