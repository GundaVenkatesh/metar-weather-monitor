package de.gvg.metarweathermonitor;

import android.content.Context;

import de.gvg.metarweathermonitor.controller.WeatherMonitorController;

import static de.gvg.metarweathermonitor.controller.WeatherMonitorController.getWeatherMonitorController;

public final class Utils {

   public static WeatherMonitorController getController(Context context){
        return getWeatherMonitorController(context);
    }
}
