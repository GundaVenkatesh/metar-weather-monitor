package de.gvg.metarweathermonitor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.gvg.metarweathermonitor.model.AirportWeatherData;

/**
 * The adapter used to show the list of {@link AirportWeatherData}s.
 */
public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.WeatherViewHolder> implements Filterable {

    private final LayoutInflater mInflater;
    private List<AirportWeatherData> mAirportWeatherDataList;
    private List<AirportWeatherData> mAirportWeatherDataFiltered;
    private OnItemClickListener mOnItemClickListener;

    /**
     * The adapter constructor.
     *
     * @param context The {@link Context} instance.
     */
    WeatherListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    /**
     * The method is used to set {@link AirportWeatherData} list to the adapter.
     *
     * @param airportWeatherDataList The {@link AirportWeatherData} list.
     */
    void setWeatherList(List<AirportWeatherData> airportWeatherDataList) {
        mAirportWeatherDataList = airportWeatherDataList;
        mAirportWeatherDataFiltered = airportWeatherDataList;
        notifyDataSetChanged();
    }

    /**
     * The listener of the item click event.
     */
    public interface OnItemClickListener {
        /**
         * Callback called when the item is clicked.
         *
         * @param airportCode The Airport Code.
         */
        void onItemClick(String airportCode);
    }

    /**
     * @param onItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.row_item, parent, false);
        return new WeatherViewHolder(itemView, mOnItemClickListener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        if (mAirportWeatherDataFiltered == null) {
            return;
        }
        AirportWeatherData airportWeatherData = mAirportWeatherDataFiltered.get(position);
        holder.airportCodeItemView.setText(airportWeatherData.getAirportCode());

        String totalTimeString = airportWeatherData.getTime();
        if (totalTimeString != null) {
            try {
                String time = totalTimeString.split("/")[0];
                holder.dateItemView.setText(time);
            } catch (Exception e) {
                holder.dateItemView.setText(totalTimeString);
            }
        }

        String areaTotalString = airportWeatherData.getLocation();
        if (areaTotalString != null) {
            if (areaTotalString.contains(airportWeatherData.getAirportCode())) {
                try {
                    String location = areaTotalString.split("\\(")[0];
                    holder.areaItemView.setText(location);
                } catch (Exception e) {
                    holder.areaItemView.setText(areaTotalString);
                }
            } else {
                holder.areaItemView.setText(areaTotalString);
            }
        }

        String tempTotalString = airportWeatherData.getTemperature();
        if (tempTotalString != null) {
            try {
                String temperature = tempTotalString.split("\\(")[1].split("\\)")[0];

                holder.temperatureItemView.setText(temperature.substring(0, temperature.length() - 1) + "°C");
            } catch (Exception e) {
                holder.temperatureItemView.setText(tempTotalString);
            }
        }

        String windTotalString = airportWeatherData.getWind();
        if (windTotalString != null) {
            try {
                String windSplitString = windTotalString.split("MPH")[0];
                if (windSplitString.contains("Calm")) {
                    holder.windItemView.setText(windSplitString);
                } else {
                    String wind = "Wind:" + windSplitString.substring(windSplitString.length() - 3) + "MPH";
                    holder.windItemView.setText(wind);
                }

            } catch (Exception e) {
                holder.windItemView.setText(airportWeatherData.getWind());
            }
        }

        String skyCondition = airportWeatherData.getSkyConditions();
        String weather = airportWeatherData.getWeather();
        if (skyCondition == null) {
            holder.weatherImageItemView.setImageResource(R.drawable.sun_clouds);
            return;
        }

        switch (skyCondition) {
            case SkyConditionConstants.OVERCAST:
                holder.weatherImageItemView.setImageResource(R.drawable.dark_cloud);
                break;
            case SkyConditionConstants.OBSCURED:
                holder.weatherImageItemView.setImageResource(R.drawable.dark_rain_light);
                break;
            case SkyConditionConstants.MOSTLY_CLOUDY:
                holder.weatherImageItemView.setImageResource(R.drawable.clouds);
                break;
            case SkyConditionConstants.CLEAR:
            case SkyConditionConstants.MOSTLY_CLEAR:
                if (airportWeatherData.getTime() != null && airportWeatherData.getTime().contains("PM")){
                    holder.weatherImageItemView.setImageResource(R.drawable.moon);
                }else {
                    holder.weatherImageItemView.setImageResource(R.drawable.sun);
                }
                break;
            case SkyConditionConstants.PARTY_CLOUDY:
            default:
                if (airportWeatherData.getTime() != null && airportWeatherData.getTime().contains("PM")){
                    holder.weatherImageItemView.setImageResource(R.drawable.moon_clouds);
                }else {
                    holder.weatherImageItemView.setImageResource(R.drawable.sun_clouds);
                }
                break;
        }

        if (weather == null) {
            return;
        }

        if (weather.contains(SkyConditionConstants.RAIN)) {
            holder.weatherImageItemView.setImageResource(R.drawable.cloud_rain);
        } else if (weather.contains(SkyConditionConstants.SNOW)) {
            holder.weatherImageItemView.setImageResource(R.drawable.snow);
        } else if (weather.contains(SkyConditionConstants.FOG)) {
            holder.weatherImageItemView.setImageResource(R.drawable.dark_cloud);
        } else if (skyCondition.contains(SkyConditionConstants.CLOUDS)) {
            holder.weatherImageItemView.setImageResource(R.drawable.clouds);
        }
    }

    @Override
    public int getItemCount() {
        if (mAirportWeatherDataFiltered != null)
            return mAirportWeatherDataFiltered.size();
        else return 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String inputString = charSequence.toString();
                if (inputString.isEmpty()) {
                    mAirportWeatherDataFiltered = mAirportWeatherDataList;
                } else {
                    List<AirportWeatherData> filteredList = new ArrayList<>();
                    for (AirportWeatherData data : mAirportWeatherDataList) {
                        if (data.getAirportCode().contains(charSequence)) {
                            filteredList.add(data);
                        }
                    }
                    mAirportWeatherDataFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mAirportWeatherDataFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mAirportWeatherDataFiltered = (List<AirportWeatherData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * The class to be extended by the  view holder used in the WeatherList Adapter,
     */
    class WeatherViewHolder extends RecyclerView.ViewHolder {
        private final TextView dateItemView;
        private final TextView airportCodeItemView;
        private final TextView areaItemView;
        private final TextView temperatureItemView;
        private final TextView windItemView;
        private final ImageView weatherImageItemView;

        /**
         * Constructor.
         *
         * @param itemView The view that contains list item view.
         */
        public WeatherViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            dateItemView = itemView.findViewById(R.id.date_row_textView);
            airportCodeItemView = itemView.findViewById(R.id.airport_code_row_textView);
            areaItemView = itemView.findViewById(R.id.area_row_textView);
            temperatureItemView = itemView.findViewById(R.id.temperature_row_textView);
            windItemView = itemView.findViewById(R.id.wind_row_textView);
            weatherImageItemView = itemView.findViewById(R.id.weather_icon_imageView);
            itemView.setOnClickListener(view -> {
                if (onItemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        String airPortCode = mAirportWeatherDataFiltered.get(position).getAirportCode();
                        onItemClickListener.onItemClick(airPortCode);
                    }
                }
            });
        }
    }
}
