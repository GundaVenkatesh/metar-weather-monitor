package de.gvg.metarweathermonitor.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.gvg.metarweathermonitor.Utils;
import de.gvg.metarweathermonitor.model.AirportWeatherData;

public class AirportWeatherDataViewModel extends AndroidViewModel {

    private LiveData<List<AirportWeatherData>> weatherDataListLiveData;

    /**
     * The Constructor.
     * @param application The {@link Application}
     */
    public AirportWeatherDataViewModel(@NonNull Application application) {
        super(application);
        weatherDataListLiveData = Utils.getController(application).getAirportWeatherLiveDataList();
    }

    /**
     * The method is used to get list of {@link AirportWeatherData} {@link LiveData}
     * @return The {@link LiveData} class is used to observe the list of the AirportWeatherData.
     * if some value is changed in the table it will notifies.
     */
   public  LiveData<List<AirportWeatherData>> getWeatherDataListLiveData() {
        return weatherDataListLiveData;
    }
}
