package de.gvg.metarweathermonitor.controller;

import java.util.HashMap;

final class AirportDataConstants {
    /**
     * The private constructor.
     */
    private AirportDataConstants() {

    }

    static final String SKY_CONDITION = "Sky conditions:";
    static final String WIND = "Wind:";
    static final String VISIBILITY = "Visibility:";
    static final String TEMPERATURE = "Temperature:";
    static final String DEW_POINT = "Dew Point:";
    static final String RELATIVE_HUMIDITY = "Relative Humidity:";
    static final String PRESSURE = "Pressure (altimeter):";
    static final String OB = "ob:";
    static final String CYCLE = "cycle:";
    static final String WEATHER = "Weather:";
    static final String GERMANY = "Germany";
    static final String EDT = "EDT";
    static final String UTC = "UTC";
    static final String STATION_NAME_NOT_AVAILABLE = "Station name not available";
    /**
     * The list of Germany airport available.
     */
    static final String[] GERMANY_AIRPORT_CODES = {"EDAC",
            "EDAH",
            "EDBC",
            "EDDB",
            "EDDC",
            "EDDE",
            "EDDF",
            "EDDG",
            "EDDH",
            "EDDI",
            "EDDK",
            "EDDL",
            "EDDM",
            "EDDN",
            "EDDP",
            "EDDR",
            "EDDS",
            "EDDT",
            "EDDV",
            "EDDW",
            "EDFE",
            "EDFH",
            "EDFM",
            "EDGS",
            "EDHI",
            "EDHK",
            "EDHL",
            "EDJA",
            "EDKA",
            "EDLN",
            "EDLP",
            "EDLV",
            "EDLW",
            "EDMA",
            "EDMB",
            "EDMO",
            "EDNY",
            "EDOP",
            "EDQM",
            "EDRZ",
            "EDSB",
            "EDTA",
            "EDTB",
            "EDTD",
            "EDTF",
            "EDTK",
            "EDTL",
            "EDTN",
            "EDTX",
            "EDTY",
            "EDVE",
            "EDVK",
            "EDXW"};

    /**
     * The method is used to get the airport with specific value.
     *
     * @return The HashMap with code and value.
     */
    public static HashMap<String, Integer> getAirportHashMap() {
        HashMap<String, Integer> airportCodeHashMap = new HashMap<>();
        Integer incrementValue = 1;
        for (String airportCode : GERMANY_AIRPORT_CODES) {
            airportCodeHashMap.put(airportCode, incrementValue);
            incrementValue++;
        }
        return airportCodeHashMap;
    }
}
