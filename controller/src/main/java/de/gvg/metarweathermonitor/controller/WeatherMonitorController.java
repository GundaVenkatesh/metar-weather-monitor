package de.gvg.metarweathermonitor.controller;

import android.content.Context;


import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import de.gvg.metarweathermonitor.model.AirportWeatherDao;
import de.gvg.metarweathermonitor.model.AirportWeatherData;
import de.gvg.metarweathermonitor.model.AirportWeatherDatabase;


/**
 * This class is in charge of managing the data.
 * The controller will keep the data persistently on database.
 */
public final class WeatherMonitorController {
    private static final String TAG = WeatherMonitorController.class.getName();
    private static volatile WeatherMonitorController INSTANCE;
    private final Executor singleThreadExecutor = Executors.newSingleThreadExecutor(new ControllerThreadFactory());
    private AirportWeatherDao mAirportWeatherDao;


    private LiveData<List<AirportWeatherData>> mAirportWeatherLiveDataList;

    private static class ControllerThreadFactory implements ThreadFactory {

        @Override
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "WeatherControllerThread");
        }
    }

    /**
     * It creates the new {@link WeatherMonitorController}instance if the instance is null or return existing {@link WeatherMonitorController}instance.
     * @param context The {@link Context} instance.
     * @return The {@link WeatherMonitorController}.
     */
    public static WeatherMonitorController getWeatherMonitorController(final Context context){
        if (INSTANCE == null){
            synchronized (WeatherMonitorController.class){
                if (INSTANCE == null){
                    INSTANCE = new WeatherMonitorController(context);
                }
            }
        }
        return INSTANCE;
    }
    /**
     * The Constructor.
     *
     * @param context A Context Instance.
     */
    private WeatherMonitorController(Context context) {
        AirportWeatherDatabase database = AirportWeatherDatabase.getDatabase(context);
        mAirportWeatherDao = database.airportWeatherDao();
        mAirportWeatherLiveDataList = mAirportWeatherDao.getAirportWeatherLiveData();
    }

    /**
     * The method is used to get the data from the metar server and insert/update in the database.
     *
     * @param onDataLoadedListener The Callback to be called when data is loaded.
     */
    @MainThread
    public void updateAirportWeatherData(final OnDataLoadedListener onDataLoadedListener) {
        final Handler handler = new Handler(Looper.getMainLooper());
        singleThreadExecutor.execute(() -> {
            try {
                for (String airportCode : AirportDataConstants.GERMANY_AIRPORT_CODES) {

                    URL url = new URL("https://tgftp.nws.noaa.gov/data/observations/metar/decoded/" + airportCode + ".TXT");
                    URLConnection con = url.openConnection();
                    InputStream in = con.getInputStream();

                    AirportWeatherData airportWeatherData = parseResponse(in, airportCode);
                    mAirportWeatherDao.insertAirPortWeatherData(airportWeatherData);
                    Log.i(TAG, "Airport Code:" + airportWeatherData.getAirportCode() + " Wind:" + airportWeatherData.getWind() + " Visibility:" + airportWeatherData.getVisibility() + " Temperature:" + airportWeatherData.getTemperature() + " Location:" + airportWeatherData.getLocation() +
                            " SkyConditions:" + airportWeatherData.getSkyConditions() + " DewPoint:" + airportWeatherData.getDewPoint() + " Pressure:" + airportWeatherData.getPressure() + " RelativeHumidity:" + airportWeatherData.getRelativeHumidity() + " Time:" + airportWeatherData.getTime() +
                            " Weather:" + airportWeatherData.getWeather() + " RawData:" + airportWeatherData.getRawData());

                }
            } catch (final Exception e) {
                Log.i(TAG, "Exception: " + e.getMessage());
                if (onDataLoadedListener != null){
                    handler.post(() -> onDataLoadedListener.onDataLoadingException(e));
                }
                return;
            }
            if (onDataLoadedListener != null){
                handler.post(onDataLoadedListener::onDataLoaded);
            }
        });
    }

    /**
     * The method is used to get the specific airport data from the metar server and insert/update in the database.
     *
     * @param airportCode          The specific airport Code.
     * @param onDataLoadedListener The Callback to be called when data is loaded.
     */
    public void updateSpecificAirportWeatherData(String airportCode, @NonNull final OnDataLoadedListener onDataLoadedListener) {
        final Handler handler = new Handler(Looper.getMainLooper());
        singleThreadExecutor.execute(() -> {
            try {
                URL url = new URL("https://tgftp.nws.noaa.gov/data/observations/metar/decoded/" + airportCode + ".TXT");
                URLConnection con = url.openConnection();
                con.setConnectTimeout(5000);
                InputStream in = con.getInputStream();
                AirportWeatherData airportWeatherData = parseResponse(in, airportCode);
                mAirportWeatherDao.insertAirPortWeatherData(airportWeatherData);
                Log.i(TAG, "Airport Code:" + airportWeatherData.getAirportCode() + " Wind:" + airportWeatherData.getWind() + " Visibility:" + airportWeatherData.getVisibility() + " Temperature:" + airportWeatherData.getTemperature() + " Location:" + airportWeatherData.getLocation() +
                        " SkyConditions:" + airportWeatherData.getSkyConditions() + " DewPoint:" + airportWeatherData.getDewPoint() + " Pressure:" + airportWeatherData.getPressure() + " RelativeHumidity:" + airportWeatherData.getRelativeHumidity() + " Time:" + airportWeatherData.getTime() +
                        " Weather:" + airportWeatherData.getWeather() + " RawData:" + airportWeatherData.getRawData());
                handler.post(onDataLoadedListener::onDataLoaded);
            } catch (final Exception e) {
                Log.i(TAG, "Exception: " + e.getMessage());
                handler.post(() -> onDataLoadedListener.onDataLoadingException(e));
            }
        });
    }

    /**
     * The method is used to parse the response and set the values to the {@link AirportWeatherData}.
     *
     * @param inputStream The inputStream response.
     * @param airportCode The specific Airport Code.
     * @return The {@link AirportWeatherData}.
     * @throws IOException Throws IOException if there is any problem with parsing.
     */
    private AirportWeatherData parseResponse(InputStream inputStream, String airportCode) throws IOException {
        Integer value = AirportDataConstants.getAirportHashMap().get(airportCode);
        AirportWeatherData airportWeatherData = new AirportWeatherData(airportCode);
        airportWeatherData.setAirportCodeIntValue(value);
        InputStreamReader isReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder sb = new StringBuilder();

        String str;
        while ((str = reader.readLine()) != null) {
            sb.append(str);
            if (str.contains(AirportDataConstants.WIND)) {
                airportWeatherData.setWind(getValue(str, AirportDataConstants.WIND));
            } else if (str.contains(AirportDataConstants.TEMPERATURE)) {
                airportWeatherData.setTemperature(getValue(str, AirportDataConstants.TEMPERATURE));
            } else if (str.contains(AirportDataConstants.VISIBILITY)) {
                airportWeatherData.setVisibility(getValue(str, AirportDataConstants.VISIBILITY));
            } else if (str.contains(AirportDataConstants.SKY_CONDITION)) {
                airportWeatherData.setSkyConditions(getValue(str, AirportDataConstants.SKY_CONDITION));
            } else if (str.contains(AirportDataConstants.DEW_POINT)) {
                airportWeatherData.setDewPoint(getValue(str, AirportDataConstants.DEW_POINT));
            } else if (str.contains(AirportDataConstants.RELATIVE_HUMIDITY)) {
                airportWeatherData.setRelativeHumidity(getValue(str, AirportDataConstants.RELATIVE_HUMIDITY));
            } else if (str.contains(AirportDataConstants.PRESSURE)) {
                airportWeatherData.setPressure(str.substring(21));
            } else if (str.contains(AirportDataConstants.WEATHER)) {
                airportWeatherData.setWeather(getValue(str, AirportDataConstants.WEATHER));
            } else if (str.contains(AirportDataConstants.OB)) {
                airportWeatherData.setRawData(getValue(str, AirportDataConstants.OB));
            } else if (str.contains(AirportDataConstants.CYCLE)) {
                String[] cycleStrings = str.split(AirportDataConstants.CYCLE);
                airportWeatherData.setCycle(getValue(str, AirportDataConstants.CYCLE));

            } else if (str.contains(AirportDataConstants.GERMANY) || str.contains(AirportDataConstants.STATION_NAME_NOT_AVAILABLE)) {
                airportWeatherData.setLocation(str);
            } else if (str.contains(AirportDataConstants.EDT) && str.contains(AirportDataConstants.UTC)) {
                airportWeatherData.setTime(str);
            }
        }
        return airportWeatherData;
    }

    /**
     * The method is used to get the value specific value from the String. It split the string based on the regex.
     *
     * @param str   The total string.
     * @param regex The regular expression which used to split the string.
     * @return The value after split.
     */
    private String getValue(String str, String regex) {
        String[] obStrings = str.split(regex);
        for (String value : obStrings) {
            if (!value.isEmpty()) {
                return value;
            }
        }
        return "";
    }

    /**
     * The method is used to get the LiveData list from the database.
     *
     * @return The List of {@link AirportWeatherData}.
     */
    public LiveData<List<AirportWeatherData>> getAirportWeatherLiveDataList() {
        return mAirportWeatherLiveDataList;
    }

    /**
     * The method is used to the clear the database.
     */
    public void deleteAirportsData() {
        singleThreadExecutor.execute(() -> mAirportWeatherDao.deleteCompleteData());
    }

    /**
     * Listener interface used to get the callback when the data is loaded.
     */
    public interface OnDataLoadedListener {
        /**
         * Callback for when the data loading is finished.
         */
        void onDataLoaded();

        /**
         * Callback when the data loading is not successfully done.
         *
         * @param exception The Exception.
         */
        void onDataLoadingException(Exception exception);
    }
}
