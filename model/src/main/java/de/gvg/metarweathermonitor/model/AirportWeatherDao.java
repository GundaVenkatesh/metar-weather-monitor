package de.gvg.metarweathermonitor.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * The Dao Class.
 */
@Dao
public interface AirportWeatherDao {
    /**
     * The method is used to insert {@link AirportWeatherData} to the database.
     *
     * @param airportWeatherData The {@link AirportWeatherData}.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAirPortWeatherData(AirportWeatherData airportWeatherData);

    /**
     * Returns the list of {@link AirportWeatherData} from the table for {@link LiveData}.
     * @return The list of {@link AirportWeatherData}
     */
    @Query("SELECT * FROM airport_weather_table ORDER BY mAirportCodeIntValue")
    LiveData<List<AirportWeatherData>> getAirportWeatherLiveData();


    /**
     * The method is used to delete complete table data.
     */
    @Query("DELETE FROM airport_weather_table")
    void deleteCompleteData();
}
