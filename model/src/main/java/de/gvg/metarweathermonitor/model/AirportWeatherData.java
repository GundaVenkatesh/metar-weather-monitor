package de.gvg.metarweathermonitor.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "airport_weather_table")
public class AirportWeatherData {

    @NonNull
    @PrimaryKey
    private String mAirportCode;
    private Integer mAirportCodeIntValue;
    private String mLocation;
    private String mTime;
    private String mWind;
    private String mVisibility;
    private String mSkyConditions;
    private String mTemperature;
    private String mDewPoint;
    private String mRelativeHumidity;
    private String mPressure;
    private String mRawData;
    private String mCycle;
    private String mWeather;


    public AirportWeatherData(@NonNull String airportCode) {
        this.mAirportCode = airportCode;
    }

    @NonNull
    public String getAirportCode() {
        return mAirportCode;
    }

    public Integer getAirportCodeIntValue() {
        return mAirportCodeIntValue;
    }

    public void setAirportCodeIntValue(Integer mAirportCodeIntValue) {
        this.mAirportCodeIntValue = mAirportCodeIntValue;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        this.mLocation = location;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }

    public String getWind() {
        return mWind;
    }

    public void setWind(String mWind) {
        this.mWind = mWind;
    }

    public String getVisibility() {
        return mVisibility;
    }

    public void setVisibility(String mVisibility) {
        this.mVisibility = mVisibility;
    }

    public String getSkyConditions() {
        return mSkyConditions;
    }

    public void setSkyConditions(String skyConditions) {
        this.mSkyConditions = skyConditions;
    }

    public String getTemperature() {
        return mTemperature;
    }

    public void setTemperature(String temperature) {
        this.mTemperature = temperature;
    }

    public String getDewPoint() {
        return mDewPoint;
    }

    public void setDewPoint(String dewPoint) {
        this.mDewPoint = dewPoint;
    }

    public String getRelativeHumidity() {
        return mRelativeHumidity;
    }

    public void setRelativeHumidity(String relativeHumidity) {
        this.mRelativeHumidity = relativeHumidity;
    }

    public void setPressure(String pressure) {
        this.mPressure = pressure;
    }

    public String getPressure() {
        return mPressure;
    }

    public void setRawData(String rawData) {
        this.mRawData = rawData;
    }

    public String getRawData() {
        return mRawData;
    }

    public String getCycle() {
        return mCycle;
    }

    public void setCycle(String cycle) {
        this.mCycle = cycle;
    }

    public void setWeather(String weather) {
        this.mWeather = weather;
    }

    public String getWeather() {
        return mWeather;
    }
}
