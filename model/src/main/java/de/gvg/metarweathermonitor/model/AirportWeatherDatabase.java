package de.gvg.metarweathermonitor.model;

import android.content.Context;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


/**
 * This class defines, using the room library, the database containing the data and status of the
 * currently performed data.
 */
@Database(entities = {AirportWeatherData.class}, version = 3,exportSchema = false)
public abstract class AirportWeatherDatabase extends RoomDatabase {
    private static  final String AIRPORT_WEATHER_DATABASE_NAME = "airport_weather_database.db";

    //The volatile variable will always be read from the main memory,
   // marking the instance as volatile to ensure atomic access to the variable
    private static volatile AirportWeatherDatabase INSTANCE;

    /**
     * @return The {@link AirportWeatherDao} that allows to access the database.
     */
    public abstract AirportWeatherDao airportWeatherDao();

    /**
     * It creates the new {@link AirportWeatherDatabase}instance if the instance is null or return existing {@link AirportWeatherDatabase}instance.
     * @param context The {@link Context} instance.
     * @return The {@link AirportWeatherDatabase}.
     */
    public static AirportWeatherDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (AirportWeatherDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AirportWeatherDatabase.class,AIRPORT_WEATHER_DATABASE_NAME).fallbackToDestructiveMigration().build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Deletes the SQLite database file.
     * @param context A {@link Context} instance.
     */
    public static void deleteDatabase(@NonNull Context context){
        context.deleteDatabase(AIRPORT_WEATHER_DATABASE_NAME);
    }



}
